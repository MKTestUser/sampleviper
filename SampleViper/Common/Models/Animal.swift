//
//  User.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import Unbox
import UIKit

enum AnimalType: String {
  case normal = "normal"
  case fighting = "fighting"
  case flying = "flying"
  case poison = "poison"
  case ground = "ground"
  case rock = "rock"
  case bug = "bug"
  case ghost = "ghost"
  case steel = "steel"
  case fire = "fire"
  case water = "water"
  case grass = "grass"
  case electric = "electric"
  case psychic = "psychic"
  case ice = "ice"
  case unknown = "unknown"
}

struct Animal {
  var id: String?
  var type: AnimalType?
  var attributes: AnimalAttributes?
  struct AnimalAttributes {
    var name: String?
    var imageUrl: String?
    var description: String?
    var height: String?
    var weight: String?
    var gender: String?
  }
  var comments: [Comment]?
  var image: UIImage?
}

extension Animal: Unboxable {
  init(unboxer: Unboxer) throws {
    let typeStr: String? = unboxer.unbox(key: "type")
    let name: String? = unboxer.unbox(keyPath: "attributes.name")
    let imageUrl: String? = unboxer.unbox(keyPath: "attributes.image-url")
    let description: String? = unboxer.unbox(keyPath: "attributes.description")
    let height: String? = unboxer.unbox(keyPath: "attributes.height")
    let weight: String? = unboxer.unbox(keyPath: "attributes.weight")
    let gender: String? = unboxer.unbox(keyPath: "attributes.gender")
    self.id = unboxer.unbox(key: "id")
    self.type = AnimalType(rawValue: typeStr ?? "")
    self.attributes = AnimalAttributes(name: name, imageUrl: imageUrl, description: description, height: height, weight: weight, gender: gender)
    self.comments = unboxer.unbox(keyPath: "relationships.comments.data")
  }
}

struct Statistic {
  let name: String?
  let unit: String?
}

struct CommentsInfo: Unboxable {
  let id: String?
  let type: String?
  let attributes: CommentAttributes?
  struct CommentAttributes {
    let email: String?
    let username: String?
  }
  
  init(unboxer: Unboxer) throws {
    self.id = unboxer.unbox(key: "id")
    self.type = unboxer.unbox(key: "type")
    
    let email: String? = unboxer.unbox(keyPath: "attributes.email")
    let username: String? = unboxer.unbox(keyPath: "attributes.username")
    self.attributes = CommentAttributes(email: email, username: username)
  }
}

struct Comment: Unboxable {
  let id: String?
  let type: String?
  let description: String?
  let date: String?
  let authorId: String?
  var username: String?
  
  init(unboxer: Unboxer) throws {
    self.id = unboxer.unbox(key: "id")
    self.type = unboxer.unbox(key: "type")
    self.description = unboxer.unbox(keyPath: "attributes.content")
   
    let unboxedDate: String? = unboxer.unbox(keyPath: "attributes.created-at")
    if let dateStr = unboxedDate {
      self.date = Date.monthDatYearDisplayFrom(dateString: dateStr)
    } else {
      self.date = ""
    }
    
    self.authorId = unboxer.unbox(keyPath: "relationships.author.data.id")
  }
}

