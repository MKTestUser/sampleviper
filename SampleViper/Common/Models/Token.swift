//
//  Token.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-10.
//  Copyright © 2018 Matej. All rights reserved.
//

import Unbox

struct Token {
  let value: String
}

extension Token: Unboxable {
  init(unboxer: Unboxer) throws {
    self.value = try unboxer.unbox(keyPath: "data.attributes.auth-token")
  }
}
