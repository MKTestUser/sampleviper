//
//  BaseViewController.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import PKHUD

class BaseViewController: UIViewController, ResponseDisplayable {  
  
  fileprivate var dimView: UIView = {
    let view = UIView.prepareForAutolayout()
    view.backgroundColor = UIColor(white: 0, alpha: 0.15)
    return view
  }()
  
  fileprivate var activityIndicator: UIActivityIndicatorView = {
    let view = UIActivityIndicatorView()
    view.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
    view.hidesWhenStopped = true
    return view
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    activityIndicator.center = self.view.center
    if activityIndicator.superview == nil {
      view.addSubview(activityIndicator)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    view.bringSubview(toFront: activityIndicator)
  }
  
  // MARK: - Loading indicator
  
  func startLoading() {
    dimView.frame = view.frame
    view.addSubview(dimView)
    view.bringSubview(toFront: dimView)
    view.bringSubview(toFront: activityIndicator)
    activityIndicator.startAnimating()
  }
  
  func stopLoading() {
    dimView.removeFromSuperview()
    activityIndicator.stopAnimating()
  }
  
  // MARK: - Show alert support
  
  func showAlert(status: OperationStatus, completion: (() -> Void)?) {
    switch status {
    case .success:
      HUD.flash(.success, onView: nil, delay: 1.0, completion: { (finished) in
        completion?()
      })
    case .failed(let error):
      let alert = UIAlertController(title: "Oops", message: error.localizedDescription, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
      present(alert, animated: true, completion: completion)
    }
  }
  
  // MARK: - Keyboard autodismissal on tap
  
  func setupKeyboardAutodismissal() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
    tap.cancelsTouchesInView = true
    view.addGestureRecognizer(tap)
  }
  
  @objc private func dismissKeyboard() {
    view.endEditing(true)
  }
}
