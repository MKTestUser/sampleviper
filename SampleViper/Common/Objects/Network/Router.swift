//
//  Router.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-10.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation
import Alamofire

class AccessTokenAdapter: RequestAdapter {
  private let accessToken: String
  private let email: String

  init(accessToken: String, email: String) {
    self.accessToken = accessToken
    self.email = email
  }
  
  func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
    var urlRequest = urlRequest
    
    if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(Router.baseURLString) {
      urlRequest.setValue("Token token=" + accessToken + "," + "email=" + email, forHTTPHeaderField: "Authorization")
    }
    
    return urlRequest
  }
}

enum Router: URLRequestConvertible {
  
  // Moves
  case getMoves
  case getSingleMove(id: String)
  
  // Comments
  case getComments(userId: String)
  case updateComment(userId: String, commentId: String)
  case viewComment(userId: String, commentId: String)
  case deleteComment(userId: String, commentId: String)
  case createComment(userId: String)
  
  // Votes
  case downvoteuser(userId: String)
  case upvoteuser(userId: String)
  
  // users
  case getAnimal
  case createAnimal(payload: Parameters)
  case upadteAnimal(userId: String)
  case aboutAnimal(userId: String)
  case deleteAnimal(userId: String)
  
  // Types
  case getTypes
  
  // Session
  case loginUser(payload: Parameters)
  case logoutUser
  
  // Users
  case getUsers
  case createUser
  case getSingleUser(userId: String)
  case checkUsername(username: String)
  case updateUser(userId: String)
  case destroyUser(userId: String)
  
  static let baseURLString = "" // FIMXME: custom API endpoint
  
  var method: HTTPMethod {
    switch self {
    case .getMoves, .getSingleMove, .getComments, .viewComment, .getAnimal, .aboutAnimal, .getTypes, .getUsers, .getSingleUser, .checkUsername:
      return .get
      
    case .updateComment, .createComment, .downvoteuser, .upvoteuser, .createAnimal, .loginUser, .createUser:
      return .post
      
    case .upadteAnimal, .updateUser:
      return .put
      
    case .deleteComment, .deleteAnimal, .logoutUser, .destroyUser:
      return .delete
    }
  }
  
  var path: String {
    switch self {
      
    // Moves
    case .getMoves:
      return "/api/v1/moves"
    case .getSingleMove(let id):
      return "/api/v1/moves/\(id)"
      
    // Comments
    case .getComments(let userId):
      return "/api/v1/users/\(userId)/comments"
    case .updateComment(let userId, let commentId):
      return "/api/v1/users/\(userId)/comments/\(commentId)"
    case .viewComment(let userId, let commentId):
      return "/api/v1/users/\(userId)/comments/\(commentId)"
    case .deleteComment(let userId, let commentId):
      return "/api/v1/users/\(userId)/comments/\(commentId)"
    case .createComment(let userId):
      return "/api/v1/users/\(userId)/comments"
      
    // Votes
    case .downvoteuser(let userId):
      return "/api/v1/users/\(userId)/downvote"
    case .upvoteuser(let userId):
      return "/api/v1/users/\(userId)/upvote"
      
    // users
    case .getAnimal:
      return "/api/v1/users"
    case .createAnimal:
      return "/api/v1/users"
    case .upadteAnimal(let userId):
      return "/api/v1/users/\(userId)"
    case .aboutAnimal(let userId):
      return "/api/v1/users/\(userId)"
    case .deleteAnimal(let userId):
      return "/api/v1/users/\(userId)"
      
    // Types
    case .getTypes:
      return "/api/v1/types"
      
    // Session
    case .loginUser:
      return "/api/v1/users/login"
    case .logoutUser:
      return "/api/v1/users/logout"
      
    // Users
    case .getUsers:
      return "/api/v1/users"
    case .createUser:
      return "/api/v1/users"
    case .getSingleUser(let userId):
      return "/api/v1/users/\(userId)"
    case .checkUsername(let username):
      return "/api/v1/users/\(username)"
    case .updateUser(let userId):
      return "/api/v1/users/\(userId)"
    case .destroyUser(let userId):
      return "/api/v1/users/\(userId)"
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try Router.baseURLString.asURL()
    
    // Request
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    // auth header payload
    let userEmail = Session.shared.activeUsername // keychain ?
    
    switch self {
    case .loginUser(let payload),
         .createAnimal(let payload):
      let data = try! JSONSerialization.data(withJSONObject: payload, options: JSONSerialization.WritingOptions.prettyPrinted)
      if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
        urlRequest.httpBody = json.data(using: String.Encoding.utf8.rawValue)
      } else {
        print("invalid JSON payload")
      }
      
      urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
  
    case .logoutUser, .getComments(_):
      urlRequest.addValue("text/html", forHTTPHeaderField: "Content-Type")
      
    case .getAnimal:
      urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)

    default:
      break
    }
  
    // All requests need to include authorization data in the header of the request, except for request for user login.
    switch self {
    case .loginUser:
      break
    default:
      urlRequest = try AccessTokenAdapter(accessToken: Session.shared.token, email: userEmail).adapt(urlRequest)
    }
    
    return urlRequest
  }
}
