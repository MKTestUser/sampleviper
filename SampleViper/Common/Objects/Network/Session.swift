//
//  Session.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-10.
//  Copyright � 2018 Matej. All rights reserved.
//

import Alamofire
import Unbox

enum NetworkError: LocalizedError {
  case noInternetConnection
  case timeout
  case server(statusCode: Int)
  case request(statusCode: Int)
  case invalidCredentials
  case invalidResponse
  case invalidJson
  case generic
  
  public var errorDescription: String? {
    switch self {
    case .noInternetConnection: return "No internet connection. Try again."
    case .timeout: return "Request timeout."
    case let .server(statusCode): return "Server error, status code: \(statusCode)!"
    case let .request(statusCode): return "Request error, status code: \(statusCode)!"
    case .invalidCredentials: return "Invalid credentials."
    case .invalidResponse: return "Invalid response."
    case .generic: return "Error."
    case .invalidJson: return "Failed parsing JSON."
    }
  }
}

final class Session {
  
  static let shared = Session()

  // MARK: - Read-only
  
  fileprivate(set) public var isValid = false {
    didSet {
      if !isValid {
        NotificationCenter.default.post(name: Notification.Name(rawValue: AppEvent.logout.rawValue), object: nil)
      }
    }
  }
  fileprivate(set) public var token = ""
  fileprivate(set) public var activeUsername = ""
  
  // MARK: - Users
  
  func login(with username: String, password: String, completion: @escaping ((_ success: Bool, _ error: Error?)->())) {

    token = "abc123"
    activeUsername = "John Doe"
    isValid = true
    completion(true, nil)
    
    activeUsername = username
    
//
//    let payload: Parameters = [
//      "data": [
//        "type" : "session",
//        "attributes": [
//          "email" : username,
//          "password": password
//        ]
//      ]
//    ]
//
//    Alamofire.request(Router.loginUser(payload: payload)).validate().response { [weak self] (response) in
//      do {
//        try self?.validate(response)
//
//        guard let data = response.data else {
//          completion(false, response.error)
//          return
//        }
//        let userToken: Token = try unbox(data: data)
//        self?.token = userToken.value
//        self?.isValid = true
//        completion(true, nil)
//      }  catch {
//        let responseError = error as? NetworkError ?? NetworkError.generic
//        self?.isValid = false
//        completion(false, responseError)
//      }
//    }
  }
  
  func logout(completion: @escaping ((_ success: Bool, _ error: Error?)->())) {
    token = ""
    activeUsername = ""
    isValid = false

    completion(true, nil)
//    Alamofire.request(Router.logoutUser).validate().response { [weak self] (response) in
//      do {
//        try self?.validate(response)
//        self?.isValid = false
//        self?.activeUsername = ""
//        completion(true, nil)
//      }  catch {
//        let responseError = error as? NetworkError ?? NetworkError.generic
//        completion(false, responseError)
//      }
//    }
  }
  
  // MARK: - Data
  
  func getMoreComments(userId: String, completion: @escaping ((_ success: Bool, _ error: Error?, _ data: Dictionary<String, Any>?)->())) {
    completion(false, NetworkError.generic, nil)
//    Alamofire.request(Router.getComments(userId: userId)).validate().response { [weak self] (response) in
//      do {
//        try self?.validate(response)
//        var json = [String: Any]()
//        if let data = response.data, let responseJson = try data.toJson() {
//          json = responseJson
//        }
//        completion(true, nil, json)
//      } catch {
//        let responseError = error as? NetworkError ?? NetworkError.generic
//        self?.isValid = false
//        completion(false, responseError, nil)
//      }
//    }
  }
  
  func getAnimals(completion: @escaping ((_ success: Bool, _ error: Error?, _ data: Dictionary<String, Any>?)->())) {
    if let path = Bundle.main.path(forResource: "getAnimals", ofType: "json") {
      do {
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
        if let jsonResult = jsonResult as? Dictionary<String, Any> {
          completion(true, nil, jsonResult)
        }
      } catch {
        print("Error reading JSON")
      }
    }
    
//    Alamofire.request(Router.getAnimal).validate().response { [weak self] (response) in
//      do {
//        try self?.validate(response)
//        var json = [String: Any]()
//        if let data = response.data, let responseJson = try data.toJson() {
//          json = responseJson
//        }
//        completion(true, nil, json)
//      } catch {
//        let responseError = error as? NetworkError ?? NetworkError.generic
//        self?.isValid = false
//        completion(false, responseError, nil)
//      }
//    }
  }
  
  func addAnimal(animal: Animal, completion: @escaping ((_ success: Bool, _ error: Error?)->())) {
    
    completion(true, nil)

//    var attributes: Parameters = [String: Any]()
//
//    if let name = animal.attributes?.name {
//      attributes["name"] = name
//    }
//    if let name = animal.attributes?.height {
//      attributes["height"] = name
//    }
//    if let name = animal.attributes?.weight {
//      attributes["weight"] = name
//    }
//    attributes["gender-id"] = "0"
//    if let name = animal.attributes?.description  {
//      attributes["description"] = name
//    }
//
//    let payload: Parameters = [
//      "data": [
//        "type" : "session",
//        "attributes": attributes
//      ]
//    ]
//
//
//    Alamofire.request(Router.createAnimal(payload: payload)).validate().response { [weak self] (response) in
//      do {
//        try self?.validate(response)
//        completion(true, nil)
//      } catch {
//        let responseError = error as? NetworkError ?? NetworkError.generic
//        self?.isValid = false
//        completion(false, responseError)
//      }
//    }
  }
}

private extension Session {
  func validate(_ response: DefaultDataResponse) throws {
    let statusCode = response.response?.statusCode ?? 0
    
    switch statusCode {
    case 401:
      throw NetworkError.invalidCredentials
    case 500..<511:
      throw NetworkError.server(statusCode: statusCode)
    case 400..<500:
      throw NetworkError.request(statusCode: statusCode)
    case 0:
      if let urlError = response.error as? URLError {
        switch urlError.code {
        case URLError.timedOut:
          throw NetworkError.timeout
        case URLError.notConnectedToInternet, URLError.networkConnectionLost:
          throw NetworkError.noInternetConnection
        default:
          throw NetworkError.generic
        }
      }
    default:
      break
    }
  }
}
