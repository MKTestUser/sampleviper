//
//  RootViewController.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import PKHUD

enum AppEvent: String {
  case login = "login"
  case logout = "logout"
  case sessionExpired = "expired"
}

final class RootViewController: UIViewController {
  
  lazy fileprivate var mainModule = UINavigationController(rootViewController: OverviewWireframe.assembleModule().viewController)
  
  // Status bar - white
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    get {
      return .lightContent
    }
  }
  
  // MARK: - View
  
  override func viewDidLoad() {
    super.viewDidLoad()
    registerForNotifications()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if !Session.shared.isValid {
      presentWireframe(LoginWireframe.assembleModule(), animated: true, completion: nil)
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Notifications
  
  fileprivate func registerForNotifications() {
    // Session
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(notificationReceived(_:)),
                                           name: NSNotification.Name(rawValue: AppEvent.logout.rawValue),
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(notificationReceived(_:)),
                                           name: NSNotification.Name(rawValue: AppEvent.login.rawValue),
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(notificationReceived(_:)),
                                           name: NSNotification.Name(rawValue: AppEvent.sessionExpired.rawValue),
                                           object: nil)
  }
  
  fileprivate func styleMainModule() {
    UINavigationBar.appearance().barTintColor = .darkBlue
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
  }
  
  // MARK: - Notifications
  
  @objc fileprivate func notificationReceived(_ notification: Notification) {
    if notification.name.rawValue == AppEvent.logout.rawValue {
      presentWireframe(LoginWireframe.assembleModule(), animated: true, completion: { [weak self] in
        self?.mainModule.setRootWireframe(OverviewWireframe.assembleModule()) // reset
      })
    }
    if notification.name.rawValue == AppEvent.login.rawValue {
      if mainModule.view.superview == nil {
        addChildViewController(mainModule)
        styleMainModule()
        view.addSubview(mainModule.view)
      }
      dismiss(animated: true, completion: nil)
    }
    if notification.name.rawValue == AppEvent.sessionExpired.rawValue {
      presentWireframe(LoginWireframe.assembleModule(), animated: true) { [weak self] in
        HUD.flash(.labeledError(title: "Error", subtitle: "Session expired"), delay: 2.5)
        self?.mainModule.setRootWireframe(OverviewWireframe.assembleModule()) // reset
      }
    }
  }
}
