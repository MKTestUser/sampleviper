//
//  ViewInterface.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import PKHUD

enum OperationStatus {
  case success
  case failed(error: Error)
}

protocol ResponseDisplayable: class {
  func showAlert(status: OperationStatus, completion: (() -> Void)?)
  func startLoading()
  func stopLoading()
}

protocol ViewInterface: class {
  func setupViews()
  func setupConstraints()
}

extension ViewInterface {
}

