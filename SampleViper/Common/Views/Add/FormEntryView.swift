//
//  FormEntryView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

enum SeparatorStyle {
  case full
  case textOnly
}

class FormEntryView: UIView {
  
  // MARK: - Public

  var isSeparatorVisible = false {
    didSet {
      separator.isHidden = !isSeparatorVisible
    }
  }
  
  // MARK: - Read only

  fileprivate(set) public var key: String?
  fileprivate(set) public var inputLabel = UITextField.prepareForAutolayout()
  
  // MARK: - Private
  
  fileprivate var separatorInsetConstraint: Constraint? = nil
  fileprivate let iconImageView = UIImageView.prepareForAutolayout()
  lazy fileprivate var separator: UIView = {
    let separator = UIView()
    separator.backgroundColor = UIColor(r: 200, g: 199, b: 204)
    separator.isHidden = true
    return separator
  }()

  // MARK: - Config
  
  func populate(with key: String, title: String, leftIcon: UIImage? = nil, separatorStyle: SeparatorStyle) {
    self.key = key
    if let icon = leftIcon {
      iconImageView.image = icon
    }
    
    // placeholder
    let font = UIFont.descriptionFont!
    let multipleAttributes: [NSAttributedStringKey : Any] = [
      NSAttributedStringKey.font: font,
      NSAttributedStringKey.foregroundColor: UIColor.lightGrayFormText ]
    let myAttrString = NSAttributedString(string: title, attributes: multipleAttributes)
    inputLabel.attributedPlaceholder = myAttrString

    // separator inset
    separatorInsetConstraint?.update(offset: separatorStyle == .full ? 0 : 56)
  }
}

// MARK: - Setup

extension FormEntryView: ViewInterface {
  func setupViews() {
    addSubview(iconImageView)
    
    addSubview(inputLabel)
    inputLabel.textAlignment = .left
    
    addSubview(separator)
  }
  
  func setupConstraints() {
    iconImageView.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(13)
      make.bottom.equalToSuperview().offset(-13)
      make.centerY.equalTo(inputLabel.snp.centerY)
      make.height.width.equalTo(24)
      make.leading.equalToSuperview().offset(16)
      make.trailing.equalTo(inputLabel.snp.leading).offset(-16)
    }
    inputLabel.snp.makeConstraints { (make) in
      make.trailing.equalToSuperview().offset(-16)
      make.top.equalToSuperview().offset(15)
      make.bottom.equalToSuperview().offset(-15)
    }
    separator.snp.makeConstraints { (make) in
      self.separatorInsetConstraint = make.leading.equalToSuperview().offset(56).constraint
      make.trailing.bottom.equalToSuperview()
      make.height.equalTo(1)
    }
  }
}
