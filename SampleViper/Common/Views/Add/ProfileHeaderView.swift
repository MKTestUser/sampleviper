//
//  ProfileHeaderView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import SnapKit
import UIKit

final class ProfileHeaderView: UIView {
  
  var delegate: ButtonActionable?
  
  // MARK: - Properties
  
  lazy fileprivate var profileImageView: UIImageView = {
    let icon = UIImageView()
    icon.image = #imageLiteral(resourceName: "image-placeholder")
    icon.contentMode = .scaleAspectFill
    icon.backgroundColor = UIColor(r: 241, g: 241, b: 241)
    return icon
  }()
  lazy fileprivate var addButton: UIButton = {
    let icon = UIButton()
    icon.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)
    icon.setBackgroundImage(#imageLiteral(resourceName: "add-image-button"), for: .normal)
    return icon
  }()
  lazy fileprivate var bottomTitle: UILabel = {
    let title = UILabel()
    title.font = UIFont.profileSubtitle
    title.tintColor = UIColor.formTitle
    title.textAlignment = .left
    title.numberOfLines = 1
    return title
  }()
  
  // MARK: - Setup
  
  func setup(with title: String) {
    bottomTitle.text = title
  }
  
  func setProfile(with image: UIImage) {
    profileImageView.image = image
  }
  
  // MARK: - Button pressed
  
  @objc fileprivate func didPressButton() {
    delegate?.didPressButton()
  }
}

// MARK: - Setup

extension ProfileHeaderView: ViewInterface {
  func setupViews() {
    clipsToBounds = true
    
    addSubview(profileImageView)
    profileImageView.contentMode = .scaleAspectFit
    
    addSubview(addButton)
    addSubview(bottomTitle)
  }
  
  func setupConstraints() {
    profileImageView.snp.makeConstraints { (make) in
      make.leading.top.trailing.equalToSuperview()
      make.height.equalTo(180)
    }
    addButton.snp.makeConstraints { (make) in
      make.trailing.equalToSuperview().offset(-20)
      make.height.width.equalTo(48)
      make.bottom.equalTo(profileImageView.snp.bottom).offset(24)
    }
    bottomTitle.snp.makeConstraints { (make) in
      make.top.equalTo(profileImageView.snp.bottom).offset(20)
      make.leading.equalToSuperview().offset(14)
      make.trailing.equalToSuperview().offset(-14)
      make.bottom.equalToSuperview()
    }
  }
}
