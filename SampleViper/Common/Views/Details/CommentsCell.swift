//
//  CommentsCell.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-09.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

class CommentsCell: UITableViewCell {
  
  lazy fileprivate var titleLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.font = UIFont.cellTitleFont
    label.textColor = .descriptionGray
    return label
  }()
  lazy fileprivate var dateLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.font = UIFont.cellTitleFont
    label.textColor = .descriptionGray
    return label
  }()
  lazy fileprivate var descriptionLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.font = UIFont.cellTitleFont
    label.textColor = .descriptionGray
    return label
  }()
  
  // MARK: - Init
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Public methods

extension CommentsCell {
  func populate(with title: String, date: String, description: String) {
    titleLabel.text = title
    dateLabel.text = date
    descriptionLabel.text = description
  }
}

// MARK: - ViewLifecycle

extension CommentsCell: ViewInterface {
  func setupViews() {
    addSubview(titleLabel)
    addSubview(dateLabel)
    addSubview(descriptionLabel)
  }
  
  func setupConstraints() {
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalTo(dateLabel.snp.leading).offset(-5)
      make.bottom.equalTo(descriptionLabel.snp.top).offset(-6)
    }
    dateLabel.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.trailing.equalToSuperview().offset(-20)
      make.bottom.equalTo(descriptionLabel.snp.top).offset(-6)
    }
    descriptionLabel.snp.makeConstraints { (make) in
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.bottom.equalToSuperview().offset(-18)
    }
  }
}
