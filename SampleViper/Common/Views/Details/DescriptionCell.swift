//
//  DescriptionCell.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-09.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

class DescriptionCell: UITableViewCell {
  lazy fileprivate var sidebar = UIView.prepareForAutolayout()
  lazy fileprivate var titleLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.font = UIFont.detailDescriptionTitle
    label.textAlignment = .left
    label.numberOfLines = 0
    return label
  }()
  lazy fileprivate var descriptionLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.font = UIFont.descriptionFont
    label.textColor = UIColor.descriptionGray
    label.numberOfLines = 0
    label.textAlignment = .left
    label.lineBreakMode = .byWordWrapping
    return label
  }()
  
  // MARK: - Init
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func populate(title: String, description: String, theme: AnimalType) {
    titleLabel.text = title
    descriptionLabel.text = description
    setTheme(theme: theme)
  }
}

fileprivate extension DescriptionCell {
  fileprivate func setTheme(theme: AnimalType) {
    sidebar.backgroundColor = UIColor.colorFor(type: theme)
    titleLabel.textColor = UIColor.colorFor(type: theme)
  }
}

// MARK: - ViewLifecycle

extension DescriptionCell: ViewInterface {
  func setupViews() {
    clipsToBounds = true
    
    addSubview(sidebar)
    addSubview(titleLabel)
    addSubview(descriptionLabel)
  }
  
  func setupConstraints() {
    sidebar.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalTo(titleLabel.snp.leading).offset(-12)
      make.bottom.equalToSuperview().offset(-19)
      make.width.equalTo(5)
    }
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.trailing.equalToSuperview().offset(-12)
      make.bottom.equalTo(descriptionLabel.snp.top).offset(-12)
    }
    descriptionLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(sidebar.snp.trailing).offset(12)
      make.trailing.equalToSuperview().offset(-12)
      make.bottom.equalToSuperview().offset(-19)
    }
  }
}
