//
//  GradientView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-12.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class GradientView: UIView {

  fileprivate let gradient: CAGradientLayer = CAGradientLayer()
    
  override init(frame: CGRect) {
    super.init(frame: frame)
    layer.masksToBounds = true
    gradient.frame = frame
    gradient.colors = [UIColor.gradientStart.cgColor, UIColor.gradientStop.cgColor]
    layer.insertSublayer(gradient, at: 0)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSublayers(of layer: CALayer) {
    super.layoutSublayers(of: layer)
    gradient.frame = bounds
  }
}

