//
//  HeaderCell.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-08.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class HeaderCell: UITableViewCell {
  fileprivate let mainImageView = UIImageView.prepareForAutolayout()
  fileprivate let gradientView = GradientView.prepareForAutolayout()
  
  // MARK: - Init
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Public methods

extension HeaderCell {
  func populate(with imageUrl: UIImage) {
    mainImageView.kf.indicatorType = .activity
    mainImageView.image = imageUrl
  }
}

// MARK: - ViewLifecycle

extension HeaderCell: ViewInterface {
  func setupViews() {
    addSubview(gradientView)

    // main image
    addSubview(mainImageView)
    mainImageView.backgroundColor = .clear
    mainImageView.contentMode = .scaleAspectFit
  }
  
  func setupConstraints() {
    mainImageView.snp.makeConstraints { (make) in
      make.edges.equalToSuperview()
      make.height.equalTo(236)
    }
    gradientView.snp.makeConstraints { (make) in
      make.edges.equalToSuperview()
    }
  }
}
