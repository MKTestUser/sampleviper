//
//  StatsCell.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-09.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

class StatsCell: UITableViewCell {
  
  fileprivate let stackView: UIStackView = {
    let stack = UIStackView.prepareForAutolayout()
    stack.axis = .vertical
    stack.distribution = .equalSpacing
    return stack
  }()

  // MARK: - Init
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func populate(with stats: [Statistic], theme: AnimalType) {
    stackView.subviews.forEach { $0.removeFromSuperview() }
    
    var row = UIStackView.prepareForAutolayout()
    row.axis = .horizontal
    row.distribution = .fillEqually
    
    for (idx, stat) in stats.enumerated() {
      // item
      let view = StatsView.prepareForAutolayout()
      view.populate(with: stat, theme: theme)
      
      // add to row
      row.addArrangedSubview(view)
      
      if idx+1 == stats.count { // end of stats
        stackView.addArrangedSubview(view)
      } else if row.subviews.count == 2 { // row full
        stackView.addArrangedSubview(row)
        row = UIStackView.prepareForAutolayout()
      }
    }
  }
}

// MARK: - ViewLifecycle

extension StatsCell: ViewInterface {
  func setupViews() {
    addSubview(stackView)
  }
  
  func setupConstraints() {
    stackView.snp.makeConstraints { (make) in
      make.top.bottom.leading.trailing.equalToSuperview()
    }
  }
}
