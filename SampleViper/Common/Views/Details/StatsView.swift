//
//  TitleDescriptionView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-09.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

class StatsView: UIView {

  private let titleLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.numberOfLines = 0
    return label
  }()
  
  private let descriptionLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.numberOfLines = 0
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Setup
  
  func populate(with statistic: Statistic, theme: AnimalType) {
    titleLabel.text = statistic.name ?? ""
    titleLabel.textColor = UIColor.colorFor(type: theme)
    descriptionLabel.text = statistic.unit ?? ""
  }
}

extension StatsView: ViewInterface {
  func setupViews() {
    addSubview(titleLabel)
    addSubview(descriptionLabel)
  }
  
  func setupConstraints() {
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(19)
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.bottom.equalTo(descriptionLabel.snp.top).offset(-4)
    }

    descriptionLabel.snp.makeConstraints { (make) in
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.bottom.equalToSuperview().offset(-19)
    }
  }
}
