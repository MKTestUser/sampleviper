//
//  TitleView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-09.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

class TitleView: UIView {
  
  lazy fileprivate var titleLabel: UILabel = {
    let label = UILabel.prepareForAutolayout()
    label.font = UIFont.commentsTitle
    label.textColor = .darkDescriptionText
    return label
  }()
  
  // MARK: - Init
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Public methods

extension TitleView {
  func populate(with title: String) {
    titleLabel.text = title
  }
}

// MARK: - ViewLifecycle

extension TitleView: ViewInterface {
  func setupViews() {
    addSubview(titleLabel)
  }
  
  func setupConstraints() {
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.bottom.equalToSuperview().offset(-17)
    }
  }
}
