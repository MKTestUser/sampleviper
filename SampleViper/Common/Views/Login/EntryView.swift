//
//  InputView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

class EntryView: UIView {
  
  // MARK: - Constants
  
  fileprivate let iconTextOffset: CGFloat = 12.0
  
  // MARK: - Public
  
  var entryField: UITextField = {
    let field = UITextField()
    field.contentVerticalAlignment = .center
    return field
  }()
  
  // MARK: - Private
  
  lazy fileprivate var leftIcon: UIImageView = {
    let icon = UIImageView()
    icon.isHidden = true
    return icon
  }()
  lazy fileprivate var rightIcon: UIButton = {
    let icon = UIButton()
    icon.addTarget(self, action: #selector(rightActionTouchedAction), for: .touchUpInside)
    icon.setBackgroundImage(#imageLiteral(resourceName: "icon-password-visibility"), for: .normal)
    icon.isHidden = true
    return icon
  }()
  lazy fileprivate var separator: UIView = {
    let separator = UIView()
    separator.backgroundColor = UIColor(r: 204, g: 204, b: 204)
    return separator
  }()
  fileprivate var entryLeading: Constraint? = nil
  fileprivate var entryTrailing: Constraint? = nil
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup(with name: String, leftIconImage: UIImage? = nil, isSecure: Bool, hasSeparator: Bool) {
    // views
    // entry
    entryField.placeholder = name
    entryField.isSecureTextEntry = isSecure
    
    // left icon
    var leftOffset: CGFloat = 0
    if let leftIconImage = leftIconImage {
      leftIcon.image = leftIconImage
      leftIcon.isHidden = false
      leftOffset = leftIconImage.size.width + iconTextOffset
    }
    self.entryLeading?.update(offset: leftOffset)
    
    // right icon
    let rightOffset: CGFloat = isSecure ? #imageLiteral(resourceName: "icon-password-visibility").size.width+iconTextOffset : 0
    rightIcon.isHidden = !isSecure
    self.entryTrailing?.update(offset: -rightOffset)
    
    // separator
    separator.isHidden = !hasSeparator
  }
  
  // MARK: - Touch
  
  @objc fileprivate func rightActionTouchedAction() {
    entryField.isSecureTextEntry = !entryField.isSecureTextEntry
    if entryField.isSecureTextEntry {
      rightIcon.setBackgroundImage(#imageLiteral(resourceName: "icon-password-visibility"), for: .normal) // TODO: get disabled icon
    }
  }
}

extension EntryView: ViewInterface {
  
  func setupViews() {
    addSubview(entryField)
    addSubview(leftIcon)
    addSubview(rightIcon)
    addSubview(separator)
  }
  
  func setupConstraints() {
    leftIcon.snp.makeConstraints { (make) in
      make.centerY.equalTo(entryField)
      make.leading.equalToSuperview()
    }
    
    entryField.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(13)
      self.entryLeading = make.leading.equalToSuperview().offset(0).constraint
      self.entryTrailing = make.trailing.equalToSuperview().offset(0).constraint
      make.bottom.equalTo(separator.snp.top).offset(-11)
    }
    
    rightIcon.snp.makeConstraints { (make) in
      make.centerY.equalTo(entryField)
      make.trailing.equalToSuperview().offset(-3)
    }
    
    separator.snp.makeConstraints { (make) in
      make.leading.trailing.bottom.equalToSuperview()
      make.height.equalTo(2)
    }
  }
}



