//
//  SplashView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit

class SplashView: UIView {
  private let titleImageView = UIImageView.prepareForAutolayout()
  private let ballImageView = UIImageView.prepareForAutolayout()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension SplashView: ViewInterface {
  func setupViews() {
    addSubview(titleImageView)
    titleImageView.contentMode = .scaleAspectFit
//    titleImageView.image =
    
    addSubview(ballImageView)
    ballImageView.contentMode = .scaleAspectFit
//    ballImageView.image =
  }
  
  func setupConstraints() {
    titleImageView.snp.makeConstraints { (make) in
      make.top.equalToSuperview()
      make.leading.equalToSuperview()
      make.trailing.equalToSuperview()
      make.bottom.equalTo(ballImageView.snp.top).offset(-11)
    }
    ballImageView.snp.makeConstraints { (make) in
      make.centerX.equalTo(titleImageView)
      make.bottom.equalToSuperview()
    }
  }
}
