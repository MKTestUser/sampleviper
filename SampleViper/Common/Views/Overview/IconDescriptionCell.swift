//
//  IconTextCell.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class IconDescriptionCell: UITableViewCell {
  var isSeparatorVisible = false {
    didSet {
      separator.isHidden = !isSeparatorVisible
    }
  }
  fileprivate let iconImageView = UIImageView.prepareForAutolayout()
  fileprivate let descriptionLabel = UILabel.prepareForAutolayout()
  lazy fileprivate var separator: UIView = {
    let separator = UIView()
    separator.backgroundColor = UIColor(r: 204, g: 204, b: 204)
    separator.isHidden = true
    return separator
  }()
  
  // MARK: - Init
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Public methods

extension IconDescriptionCell {
  func populate(with animal: Animal) {
    iconImageView.kf.indicatorType = .activity
    iconImageView.image = animal.image
    descriptionLabel.text = animal.attributes?.name ?? ""
  }
}

// MARK: - ViewLifecycle

extension IconDescriptionCell: ViewInterface {
  func setupViews() {
    addSubview(iconImageView)
    iconImageView.layer.masksToBounds = true
    iconImageView.layer.cornerRadius = 20
    iconImageView.layer.borderColor = UIColor.lightGray.cgColor
    iconImageView.layer.borderWidth = 1.0
    
    addSubview(descriptionLabel)
    descriptionLabel.textAlignment = .left
    descriptionLabel.numberOfLines = 0
    
    addSubview(separator)
  }
  
  func setupConstraints() {
    iconImageView.snp.makeConstraints { (make) in
      make.top.equalToSuperview().offset(8)
      make.bottom.equalToSuperview().offset(-8)
      make.height.width.equalTo(40)
      make.leading.equalToSuperview().offset(11)
      make.trailing.equalTo(descriptionLabel.snp.leading).offset(-11)
    }
    descriptionLabel.snp.makeConstraints { (make) in
      make.trailing.equalToSuperview().offset(-23)
      make.top.equalToSuperview().offset(8)
      make.bottom.equalToSuperview().offset(-8)
    }
    separator.snp.makeConstraints { (make) in
      make.leading.trailing.bottom.equalToSuperview()
      make.height.equalTo(1)
    }
  }
}

