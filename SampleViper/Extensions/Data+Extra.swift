//
//  Data+Extra.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-11.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation

extension Data {
  func toJson() throws -> [String: Any]? {
    return try JSONSerialization.jsonObject(with: self, options: []) as? [String: Any]
  }
}
