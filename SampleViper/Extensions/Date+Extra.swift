//
//  Date+Extra.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-11.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation

extension Date {
  
  static func monthDatYearDisplayFrom(dateString: String) -> String? {
    let formatter = DateFormatter()
    formatter.locale = Locale.init(identifier: "en_US_POSIX")
    formatter.dateFormat = "yyyy-MM-dd'T'HH:m:s.SSSZ"

    var str = ""
    if let date = formatter.date(from: dateString) {
      formatter.dateFormat = "Y-MMM-d"
      str = formatter.string(from: date)
      
      let year = str.components(separatedBy: "-")[0]
      let month = str.components(separatedBy: "-")[1]
      
      var day = str.components(separatedBy: "-")[2]
      day = day.count == 1 ? "0"+day : day
      
      str = month + " " + day + ", " + year
    }
    return str
  }
}
