//
//  UIColor+Extra.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1) {
    self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
  }
}

// MARK: - App Colors

extension UIColor {
  // login
  static let formText = UIColor(r: 119, g: 119, b: 119)
  static let darkBlue = UIColor(r: 49, g: 78, b: 143)
  
  // overview
  static let darkDescriptionText = UIColor(r: 53, g: 53, b: 53, a:87)
  
  // add
  static let formTitle = UIColor(r: 109, g: 109, b: 114)
  static let lightGrayFormText = UIColor(r: 199, g: 199, b: 205)

  // detail screen
  static let descriptionGray = UIColor(r: 85, g: 85, b: 85)
  
  // gradient start
  static let gradientStart = UIColor(r: 186, g: 186, b: 186)
  static let gradientStop = UIColor(r: 0, g: 0, b: 0, a: 0)

  // Animal
  struct AnimalTypeColor {
    static let normal = UIColor(r: 151, g: 153, b: 101)
    static let fire = UIColor(r: 234, g: 107, b: 37)
    static let fight = UIColor(r: 177, g: 29, b: 31)
    static let water = UIColor(r: 86, g: 121, b: 236)
    static let fly = UIColor(r: 151, g: 119, b: 236)
    static let grass = UIColor(r: 104, g: 193, b: 63)
    static let poison = UIColor(r: 141, g: 39, b: 142)
    static let electric = UIColor(r: 245, g: 200, b: 37)
    static let ground = UIColor(r: 216, g: 180, b: 86)
    static let psychic = UIColor(r: 244, g: 61, b: 117)
    static let rock = UIColor(r: 169, g: 145, b: 43)
    static let ice = UIColor(r: 136, g: 208, b: 207)
    static let bug = UIColor(r: 152, g: 173, b: 25)
    static let dragon = UIColor(r: 91, g: 16, b: 246)
    static let ghost = UIColor(r: 92, g: 66, b: 134)
    static let dark = UIColor(r: 92, g: 70, b: 56)
    static let steel = UIColor(r: 169, g: 168, b: 197)
    static let fairy = UIColor(r: 232, g: 132, b: 156)
    static let unknown = UIColor.black

  }
  
  static func colorFor(type: AnimalType) -> UIColor {
    switch type {
    case .normal: return UIColor.AnimalTypeColor.normal
    case .fighting: return UIColor.AnimalTypeColor.fight
    case .flying: return UIColor.AnimalTypeColor.fly
    case .poison: return UIColor.AnimalTypeColor.poison
    case .ground: return UIColor.AnimalTypeColor.ground
    case .rock: return UIColor.AnimalTypeColor.rock
    case .bug: return UIColor.AnimalTypeColor.bug
    case .ghost: return UIColor.AnimalTypeColor.ghost
    case .steel: return UIColor.AnimalTypeColor.steel
    case .fire: return UIColor.AnimalTypeColor.fire
    case .water: return UIColor.AnimalTypeColor.water
    case .grass: return UIColor.AnimalTypeColor.grass
    case .electric: return UIColor.AnimalTypeColor.electric
    case .psychic: return UIColor.AnimalTypeColor.psychic
    case .ice: return UIColor.AnimalTypeColor.ice
    case .unknown: return UIColor.AnimalTypeColor.unknown
    }
  }

}
