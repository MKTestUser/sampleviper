//
//  UIFont+Extra.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

extension UIFont {
  static let descriptionFont = UIFont(name: "Helvetica", size: 17.0)
  static let cellTitleFont = UIFont(name: "Helvetica", size: 16.0)
  
  static let profileSubtitle = UIFont(name: "Helvetica", size: 13.0)
  
  static let detailDescriptionTitle = UIFont(name: "Helvetica", size: 22.0)
  static let commentsTitle = UIFont(name: "Helvetica", size: 20.0)
}
