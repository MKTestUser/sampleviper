//
//  UIView+Extra.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

extension UIView {
  class func prepareForAutolayout() -> Self {
    let instance = self.init()
    instance.translatesAutoresizingMaskIntoConstraints = false
    return instance
  }
  
  func addZeroConstraintsView(_ containedView: UIView,
                              constraintPriorityHorizontal: UILayoutPriority = UILayoutPriority.required,
                              constraintPriorityVertical: UILayoutPriority = UILayoutPriority.required) {
    addConstraintsForContainedView(containedView, insets: UIEdgeInsets.zero, constraintPriorityHorizontal: constraintPriorityHorizontal, constraintPriorityVertical: constraintPriorityVertical)
  }
  
  func addConstraintsForContainedView(_ containedView: UIView,
                                      insets: UIEdgeInsets,
                                      constraintPriorityHorizontal: UILayoutPriority = UILayoutPriority.required,
                                      constraintPriorityVertical: UILayoutPriority = UILayoutPriority.required) {
    addSubview(containedView)
    if containedView.translatesAutoresizingMaskIntoConstraints {
      containedView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    let dictionary = ["containedView": containedView]
    let horizontalFormat = "H:|-(\(insets.left)@\(constraintPriorityHorizontal.rawValue))-[containedView]-(\(insets.right)@\(constraintPriorityHorizontal.rawValue))-|"
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat,
                                                  options: [],
                                                  metrics: nil,
                                                  views: dictionary))
    
    
    let verticalFormat = "V:|-(\(insets.top)@\(constraintPriorityVertical.rawValue))-[containedView]-(\(insets.bottom)@\(constraintPriorityVertical.rawValue))-|"
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalFormat,
                                                  options: [],
                                                  metrics: nil,
                                                  views: dictionary))
  }
}
