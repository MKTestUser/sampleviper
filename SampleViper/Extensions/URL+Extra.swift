//
//  URL+Extra.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-11.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

extension URL {
  func asCustomURL() -> URL? {
    return URL(string: Router.baseURLString+self.absoluteString)
  }
}
