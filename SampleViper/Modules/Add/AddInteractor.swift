//
//  AddInteractor.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import Unbox

final class AddInteractor: AddInteractorInterface {
  
  func save(animal: Animal, completion: @escaping ((_ success: Bool, _ error: Error?)->())) {
    Session.shared.addAnimal(animal: animal, completion: completion)
  }
  
  func takePhoto() {
    // TODO:
//    UIImagePickerController.
  }
  
  
}
