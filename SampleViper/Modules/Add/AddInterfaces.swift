//
//  AddInterfaces.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation

enum AddNavigationOption {
}

protocol AddWireframeInterface: WireframeInterface {
  func dismissViewController()
}

protocol AddViewInterface: ViewInterface, ResponseDisplayable {
}

protocol AddPresenterInterface: PresenterInterface {
  func save(animal: Animal)
  func takePhoto()
  func dismissViewController()
}

protocol AddInteractorInterface: InteractorInterface {
  func save(animal: Animal, completion: @escaping ((_ success: Bool, _ error: Error?)->())) 
  func takePhoto()
}

protocol ButtonActionable {
  func didPressButton()
}
