//
//  AddPresenter.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation
import PKHUD

final class AddPresenter {
  
  // MARK: - Private properties -
  
  fileprivate unowned let _view: AddViewInterface
  fileprivate let _interactor: AddInteractorInterface
  fileprivate let _wireframe: AddWireframeInterface
  
  // MARK: - Lifecycle -
  
  init(wireframe: AddWireframeInterface, view: AddViewInterface, interactor: AddInteractorInterface) {
    _wireframe = wireframe
    _view = view
    _interactor = interactor
  }
}

extension AddPresenter: AddPresenterInterface {
  
  // MARK: - User interaction
  
  func save(animal: Animal) {
    _interactor.save(animal: animal) { [weak self] (success, error) in
      if success {
        self?._view.showAlert(status: OperationStatus.success, completion: {
          NotificationCenter.default.post(name: Notification.Name(rawValue: AppEvent.login.rawValue), object: nil)
        })
      } else {
        self?._view.showAlert(status: OperationStatus.failed(error: error ?? NetworkError.generic), completion: nil)
      }
    }
  }
  
  func takePhoto() {
    _interactor.takePhoto()
  }
  
  // MARK: - View
  
  func dismissViewController() {
    _wireframe.dismissViewController()
  }
  
}
