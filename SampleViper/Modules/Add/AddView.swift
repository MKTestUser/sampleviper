//
//  AddView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

class AddView: BaseViewController {

  enum AddFormKeys: String {
    case name = "Name"
    case height = "Height"
    case weight = "Weight"
    case type = "Type"
    case description = "Description"
  }
  
  var presenter: AddPresenterInterface!
  
  lazy fileprivate var animalHeaderView: ProfileHeaderView = {
    let header = ProfileHeaderView.prepareForAutolayout()
    header.setup(with: "ANIMAL INFO")
    return header
  }()
  lazy fileprivate var formView: UIStackView = {
    let stack = UIStackView.prepareForAutolayout()
    stack.axis = .vertical
    stack.alignment = .fill
    stack.spacing = 0.0
    stack.distribution = .equalSpacing
    return stack
  }()
  lazy fileprivate var saveButton: UIButton = {
    let button = UIButton.prepareForAutolayout()
    button.backgroundColor = .darkBlue
    button.setTitle("Save", for: .normal)
    button.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
    return button
  }()
  lazy fileprivate var closeButton: UIButton = {
    let button = UIButton.prepareForAutolayout()
    button.backgroundColor = .darkBlue
    button.setTitle("Close", for: .normal)
    button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
    return button
  }()
  lazy fileprivate var scrollView: UIScrollView = {
    let scrollView = UIScrollView.prepareForAutolayout()
    scrollView.addSubview(animalHeaderView)
    scrollView.addSubview(formView)
    return scrollView
  }()
  
  // MARK: - View
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupKeyboardAutodismissal()
    setupViews()
    setupConstraints()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    registerForNotifications()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Notifications
  
  private func registerForNotifications() {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillShow(_:)),
      name: .UIKeyboardWillShow,
      object: nil
    )
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillHide(_:)),
      name: .UIKeyboardWillHide,
      object: nil
    )
  }
  
  @objc func keyboardWillShow(_ notification: Notification) {
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
      scrollView.contentInset.bottom = keyboardSize.height
    } else {
      debugPrint("Showing keyboard, keyboard size or window is nil.")
    }
  }
  
  @objc func keyboardWillHide(_ notification: Notification) {
    scrollView.contentInset.bottom = 0
  }
  
  // MARK: - User interaction
  
  @objc fileprivate func closeAction() {
    presenter.dismissViewController()
  }
  
  @objc fileprivate func saveAction() {
    
    var type: AnimalType?
    var attributes = Animal.AnimalAttributes(name: "", imageUrl: "", description: "", height: "", weight: "", gender: "")

    // form data
    for case let entry as FormEntryView in formView.subviews {
      guard let key = entry.key else {
        continue
      }
      switch key {
      case AddFormKeys.name.rawValue:
        attributes.name = entry.inputLabel.text ?? ""
        
      case AddFormKeys.height.rawValue:
        attributes.height = entry.inputLabel.text ?? ""

      case AddFormKeys.weight.rawValue:
        attributes.weight = entry.inputLabel.text ?? ""

      case AddFormKeys.type.rawValue:
        type = AnimalType(rawValue: entry.inputLabel.text ?? "")

      case AddFormKeys.description.rawValue:
        attributes.description = entry.inputLabel.text ?? ""
     
      default:
        break
      }
    }
    
    let animal = Animal(id: "", type: type, attributes: attributes, comments: nil, image: nil)
    presenter.save(animal: animal)
  }
}

extension AddView: AddViewInterface {
  func setupViews() {
    view.backgroundColor = .white
    
    // header
    animalHeaderView.setupViews()
    
    // navigation
    navigationItem.title = "Add new Animal"
    
    // form
    let fields = [(AddFormKeys.name.rawValue, #imageLiteral(resourceName: "icon-person")),
                  (AddFormKeys.height.rawValue, nil),
                  (AddFormKeys.weight.rawValue, nil),
                  (AddFormKeys.type.rawValue, nil),
                  (AddFormKeys.description.rawValue, #imageLiteral(resourceName: "icon-description"))]
    for field in fields {
      let form = FormEntryView.prepareForAutolayout()
      form.setupViews()
      form.setupConstraints()
      form.isSeparatorVisible = true
      form.populate(with: field.0, title: field.0, leftIcon: field.1, separatorStyle: field.0 == "Description" ? .full : .textOnly)
      formView.addArrangedSubview(form)
    }
    
    view.addSubview(scrollView)
    view.addSubview(saveButton)
    view.addSubview(closeButton)
  }
  
  func setupConstraints() {
    scrollView.snp.makeConstraints { (make) in
      make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
      make.leading.trailing.equalToSuperview()
      make.bottom.equalTo(saveButton.snp.top)
    }
    animalHeaderView.setupConstraints()
    animalHeaderView.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: UILayoutConstraintAxis.vertical)
    animalHeaderView.snp.makeConstraints { (make) in
      make.top.leading.trailing.equalToSuperview()
      make.bottom.equalTo(formView.snp.top)
    }
    formView.setContentHuggingPriority(UILayoutPriority.defaultLow, for: UILayoutConstraintAxis.vertical)
    formView.snp.makeConstraints { (make) in
      make.leading.trailing.equalToSuperview()
      make.bottom.equalToSuperview().offset(10)
      make.width.equalTo(scrollView.snp.width)
    }
    saveButton.snp.makeConstraints { (make) in
      make.leading.trailing.equalToSuperview()
      make.bottom.greaterThanOrEqualTo(closeButton.snp.top).offset(-24)
      make.bottom.lessThanOrEqualTo(closeButton.snp.top).offset(-1)
      make.height.equalTo(49)
    }
    closeButton.snp.makeConstraints { (make) in
      make.leading.trailing.equalToSuperview()
      make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
      make.height.equalTo(49)
    }
  }
}
