//
//  AddWireframe.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class AddWireframe: BaseWireframe {
  
  // MARK: - Module setup
  
  static func assembleModule() -> BaseWireframe {
    let view = AddView()
    let interactor = AddInteractor()
    let router = AddWireframe(viewController: view)
    let presenter = AddPresenter(wireframe: router, view: view, interactor: interactor)
    // view
    view.presenter = presenter
    return router
  }
}

// MARK: - Extensions -
extension AddWireframe: AddWireframeInterface {
  func dismissViewController() {
    viewController.dismiss(animated: true, completion: nil)
  }
}
