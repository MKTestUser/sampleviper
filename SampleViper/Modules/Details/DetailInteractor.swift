//
//  DetailInteractor.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-08.
//  Copyright © 2018 Matej. All rights reserved.
//

import Unbox

final class DetailInteractor: DetailInteractorInterface {
  func loadComments(animalId: String, completion: @escaping ((_ success: Bool, _ error: Error?, _ comments: [Comment]?)->())) {
    Session.shared.getMoreComments(userId: animalId) { (success, error, json) in
      do {
        guard let json = json else {
          completion(false, error, nil)
          return
        }
        
        var responseComments: [Comment] = try unbox(dictionary: json, atKey: "data")
        let commentsInfo: [CommentsInfo] = try unbox(dictionary: json, atKey: "included")
        // map usernames for comment author id
        for (idx, comment) in responseComments.enumerated() {
          if let info = commentsInfo.first(where: { $0.id == comment.authorId }) {
            responseComments[idx].username = info.attributes?.username
          }
        }
        
        completion(true, nil, responseComments)
      } catch let error {
        completion(false, error, nil)
      }
    }
  }
}
