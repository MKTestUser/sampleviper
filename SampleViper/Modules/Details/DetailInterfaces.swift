//
//  DetailInterfaces.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-08.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation


enum DetailNavigationOption {
}

protocol DetailWireframeInterface: WireframeInterface {
  
}

protocol DetailViewInterface: ViewInterface, ResponseDisplayable {
  func reload()
}

protocol DetailPresenterInterface: PresenterInterface {
  func popViewController()
  func numberOfStats() -> Int?
  func getAnimal() -> Animal?
  func getComments() -> [Comment]?
  func loadMoreComments()
  func getStats() -> [Statistic]? 
}

protocol DetailInteractorInterface: InteractorInterface {
  func loadComments(animalId: String, completion: @escaping ((_ success: Bool, _ error: Error?, _ comments: [Comment]?)->()))
}
