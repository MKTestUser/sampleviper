//
//  DetailPresenter.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-08.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class DetailPresenter {
  
  // MARK: - Private properties -
  
  fileprivate unowned let _view: DetailViewInterface
  fileprivate let _interactor: DetailInteractorInterface
  fileprivate let _wireframe: DetailWireframeInterface
  
  var animal: Animal?
  var comments: [Comment]?
  
  // MARK: - Lifecycle -
  
  init(wireframe: DetailWireframeInterface, view: DetailViewInterface, interactor: DetailInteractorInterface) {
    _wireframe = wireframe
    _view = view
    _interactor = interactor
  }
}

extension DetailPresenter: DetailPresenterInterface {
  
  // MARK: - View controller
  
  func popViewController() {
    (_wireframe as? BaseWireframe)?.navigationController?.popViewController(animated: true)
  }
  
  // MARK: - Table view
  
  func numberOfRows() -> Int {
    return 4 // TODO: should be based on json fields
  }
  
  // MARK: - Data
  
  func numberOfStats() -> Int? {
    return 5 // TODO: should be based on json fields
  }
  
  func getAnimal() -> Animal? {
    return animal
  }
  
  func getStats() -> [Statistic]? {
    return [Statistic(name: AddView.AddFormKeys.height.rawValue, unit: animal?.attributes?.height),
            Statistic(name: AddView.AddFormKeys.weight.rawValue, unit: animal?.attributes?.weight),
            Statistic(name: "Gender", unit: animal?.attributes?.gender),
            Statistic(name: AddView.AddFormKeys.type.rawValue, unit: animal?.type?.rawValue)]
  }
  
  func getComments() -> [Comment]? {
    return comments
  }
  
  // MARK: - Data

  func loadMoreComments() {
    _interactor.loadComments(animalId: animal?.id ?? "") { [weak self] (success, error, comments) in
      if success {
        self?.comments = comments
        self?._view.reload()
      } else {
        self?._view.showAlert(status: OperationStatus.failed(error: error ?? NetworkError.generic), completion: nil)
      }
    }
  }  
}
