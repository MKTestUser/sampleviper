//
//  DetailView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-08.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class DetailView: BaseViewController {
  var presenter: DetailPresenterInterface!
  
  // TODO: conform to swift 4.2 protocol and remove "case all"
  fileprivate enum ViewSections: Int {
    case header = 0
    case description
    case stats
    case comments
    case all
  }
  
  fileprivate var statsSize: CGFloat = UITableViewAutomaticDimension
  lazy fileprivate var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .grouped)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 140
    tableView.estimatedSectionHeaderHeight = 50
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.allowsSelection = false
    tableView.backgroundColor = .white
    tableView.separatorColor = .clear
    tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(HeaderCell.self, forCellReuseIdentifier: HeaderCell.self.description())
    tableView.register(DescriptionCell.self, forCellReuseIdentifier: DescriptionCell.self.description())
    tableView.register(StatsCell.self, forCellReuseIdentifier: StatsCell.self.description())
    tableView.register(CommentsCell.self, forCellReuseIdentifier: CommentsCell.self.description())
    return tableView
  }()
  
  // MARK: - View
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
  }
}

// MARK: - UITableViewDelegate

extension DetailView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if section == ViewSections.comments.rawValue {
      let view = TitleView(frame: .zero)
      view.populate(with: "Comments")
      return view
    } else {
      return nil
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == ViewSections.comments.rawValue {
      return UITableViewAutomaticDimension
    } else {
      return CGFloat.leastNormalMagnitude
    }
  }
}

// MARK: - UITableViewDataSource

extension DetailView: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return ViewSections.all.rawValue
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == ViewSections.comments.rawValue {
      return presenter.getComments()?.count ?? 0
    } else {
      return 1
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.section == ViewSections.comments.rawValue && indexPath.row == (presenter.getComments()?.count ?? 0)-1 {
      presenter.loadMoreComments()
    }
    
    let cell = UITableViewCell()
    if indexPath.section == ViewSections.comments.rawValue {
      // COMMENTS
      guard let comment = presenter.getComments()?[safe: indexPath.row],
        let commentsCell = tableView.dequeueReusableCell(withIdentifier: CommentsCell.self.description(), for: indexPath) as? CommentsCell else {
        return cell
      }
      commentsCell.populate(with: comment.username ?? "", date: comment.date ?? "", description: comment.description ?? "")
      return commentsCell
    } else {
      switch indexPath.section {
      // HEADER
      case ViewSections.header.rawValue:
        guard let headerImageUrl = presenter.getAnimal()?.image,
          let headerCell = tableView.dequeueReusableCell(withIdentifier: HeaderCell.self.description(), for: indexPath) as? HeaderCell else {
          return cell
        }
        headerCell.populate(with: headerImageUrl)
        return headerCell
        
      // DESCRIPTION
      case ViewSections.description.rawValue:
        guard let descriptionCell = tableView.dequeueReusableCell(withIdentifier: DescriptionCell.self.description(), for: indexPath) as? DescriptionCell else {
          return cell
        }
        descriptionCell.populate(title: presenter.getAnimal()?.attributes?.name ?? "",
                                 description: presenter.getAnimal()?.attributes?.description ?? "",
                                 theme: presenter.getAnimal()?.type ?? AnimalType.unknown)
        return descriptionCell
      // STATS
      case ViewSections.stats.rawValue:
        guard let stats = presenter.getStats(),
          let statsCell = tableView.dequeueReusableCell(withIdentifier: StatsCell.self.description(), for: indexPath) as? StatsCell else {
          return cell
        }
        statsCell.populate(with: stats, theme: presenter.getAnimal()?.type ?? AnimalType.unknown)
        return statsCell
      default:
        return cell
      }
    }
  }
}

extension DetailView: DetailViewInterface {

  @objc func backButtonPressed() {
    presenter.popViewController()
  }
  
  // MARK: - Refresh

  func reload() {
    tableView.reloadData()
  }
  
  // MARK: - Setup
  
  func setupViews() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-back"), style: .plain, target: self, action: #selector(backButtonPressed))
    navigationItem.leftBarButtonItem?.tintColor = .white
    
    title = presenter.getAnimal()?.attributes?.name ?? ""
    view.addSubview(tableView)
  }
  
  func setupConstraints() {
    tableView.snp.makeConstraints { (make) in
      make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
      make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
      make.leading.trailing.equalToSuperview()
    }
  }
}
