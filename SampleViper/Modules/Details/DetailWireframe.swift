//
//  DetailWireframe.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-08.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class DetailWireframe: BaseWireframe {
  
  // MARK: - Module setup
  
  static func assembleModule(with item: Animal) -> BaseWireframe {
    let view = DetailView()
    let interactor = DetailInteractor()
    let router = DetailWireframe(viewController: view)
    let presenter = DetailPresenter(wireframe: router, view: view, interactor: interactor)
    presenter.animal = item
    presenter.comments = item.comments
    // view
    view.presenter = presenter
    return router
  }
}

// MARK: - Extensions -
extension DetailWireframe: DetailWireframeInterface {
  func navigate(to option: DetailNavigationOption) {
  }
}
