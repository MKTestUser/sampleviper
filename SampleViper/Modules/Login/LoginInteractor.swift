//
//  LoginInteractor.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation

final class LoginInteractor {
}

extension LoginInteractor: LoginInteractorInterface {
  func login(with username: String, password: String, completion: @escaping ((_ success: Bool, _ error: Error?)->())) {
    Session.shared.login(with: username, password: password, completion: completion)
  }
}
