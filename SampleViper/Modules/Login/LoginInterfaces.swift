//
//  LoginInterfaces.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation

enum LoginNavigationOption {
}

protocol LoginWireframeInterface: WireframeInterface {
  func navigate(to option: LoginNavigationOption)
}

protocol LoginViewInterface: ViewInterface, ResponseDisplayable {
}

protocol LoginPresenterInterface: PresenterInterface {
  func loginWith(username: String, password: String)
}

protocol LoginInteractorInterface: InteractorInterface {
  func login(with username: String, password: String, completion: @escaping ((_ success: Bool, _ error: Error?)->()))
}
