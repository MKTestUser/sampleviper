//
//  LoginPresenter.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import PKHUD

final class LoginPresenter {
  
  // MARK: - Private properties -
  
  private unowned let _view: LoginViewInterface
  private let _interactor: LoginInteractorInterface
  private let _wireframe: LoginWireframeInterface
  
  // MARK: - Lifecycle -
  
  init(wireframe: LoginWireframeInterface, view: LoginViewInterface, interactor: LoginInteractorInterface) {
    _wireframe = wireframe
    _view = view
    _interactor = interactor
  }
}

// MARK: - Extensions -

extension LoginPresenter: LoginPresenterInterface {
  func loginWith(username: String, password: String) {
    _interactor.login(with: username, password: password) { [weak self] (success, error) in
      if success {
        self?._view.showAlert(status: OperationStatus.success, completion: {
          NotificationCenter.default.post(name: Notification.Name(rawValue: AppEvent.login.rawValue), object: nil)
        })
      } else {
        self?._view.showAlert(status: OperationStatus.failed(error: error ?? NetworkError.generic), completion: nil)
      }
    }
  }
}
