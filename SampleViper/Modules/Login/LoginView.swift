//
//  ViewController.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class LoginView: BaseViewController {
  lazy private var mainView = SplashView.prepareForAutolayout()
  lazy private var emailInputView = EntryView.prepareForAutolayout()
  lazy private var passwordInputView = EntryView.prepareForAutolayout()
  lazy private var loginButton: UIButton = {
    let button = UIButton()
    button.backgroundColor = .darkBlue
    button.setTitle("Log in", for: .normal)
    button.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
    return button
  }()

  // MARK: - Public properties -
  
  var presenter: LoginPresenterInterface!
  
  // MARK: - Life cycle -
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
    setupKeyboardAutodismissal()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    upadteLoginButton()
    registerForNotifications()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Notifications
  
  private func registerForNotifications() {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillShow(_:)),
      name: .UIKeyboardWillShow,
      object: nil
    )
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillHide(_:)),
      name: .UIKeyboardWillHide,
      object: nil
    )
  }
  
  @objc func keyboardWillShow(_ notification: Notification) {
    let userInfo = notification.userInfo!
    let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
    
    view.layoutIfNeeded()
    loginButton.snp.updateConstraints { (make) in
      make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-keyboardHeight)
    }
    view.setNeedsUpdateConstraints()
    
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
      self.mainView.alpha = 0.0
    }
  }
  
  @objc func keyboardWillHide(_ notification: Notification) {
    view.layoutIfNeeded()
    loginButton.snp.updateConstraints { (make) in
      make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(0)
    }
    view.setNeedsUpdateConstraints()
    
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
      self.mainView.alpha = 1.0
    }
  }
  
  fileprivate func upadteLoginButton() {
    if let email = emailInputView.entryField.text, let pass = passwordInputView.entryField.text, !email.isEmpty && !pass.isEmpty {
      loginButton.isUserInteractionEnabled = true
      loginButton.backgroundColor = .darkBlue
    } else {
      loginButton.isUserInteractionEnabled = false
      loginButton.backgroundColor = .lightGray
    }
  }
  
  // MARK: - Actionable
  
  @objc fileprivate func loginAction() {
    presenter.loginWith(username: emailInputView.entryField.text ?? "", password: passwordInputView.entryField.text ?? "")
  }
}

// MARK: - Extensions -

extension LoginView: UITextFieldDelegate {
  func textFieldDidEndEditing(_ textField: UITextField) {
    upadteLoginButton()
  }
}

extension LoginView: LoginViewInterface {
  
  func setupViews() {
    view.backgroundColor = .white

    view.addSubview(mainView)
    mainView.setupViews()
    
    view.addSubview(emailInputView)
    emailInputView.setup(with: "E-mail", leftIconImage: #imageLiteral(resourceName: "icon-email"), isSecure: false, hasSeparator: true)
    emailInputView.entryField.delegate = self
    
    view.addSubview(passwordInputView)
    passwordInputView.setup(with: "Password", leftIconImage: #imageLiteral(resourceName: "icon-password"), isSecure: true, hasSeparator: true)
    passwordInputView.entryField.delegate = self
    
    view.addSubview(loginButton)
    loginButton.isUserInteractionEnabled = false
    loginButton.backgroundColor = .lightGray
  }
  
  func setupConstraints() {
    mainView.setupConstraints()

    mainView.snp.makeConstraints { (make) in
      make.centerX.equalToSuperview()
      make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(66)
    }
    emailInputView.snp.makeConstraints { (make) in
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.height.equalTo(48)
      make.bottom.equalTo(passwordInputView.snp.top).offset(-12)
    }
    passwordInputView.snp.makeConstraints { (make) in
      make.bottom.equalTo(loginButton.snp.top).offset(-42)
      make.leading.equalToSuperview().offset(20)
      make.trailing.equalToSuperview().offset(-20)
      make.height.equalTo(48)
    }
    loginButton.snp.makeConstraints { (make) in
      make.leading.trailing.equalToSuperview()
      make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
      make.height.equalTo(49)
    }
  }
}
