//
//  LoginWireframe.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-06.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class LoginWireframe: BaseWireframe {
  
  // MARK: - Module setup -
  
  static func assembleModule() -> BaseWireframe {
    let view = LoginView()
    let interactor = LoginInteractor()
    let router = LoginWireframe(viewController: view)
    let presenter = LoginPresenter(wireframe: router, view: view, interactor: interactor)
    // view
    view.presenter = presenter
    return router
  }
  
}

// MARK: - Extensions -
extension LoginWireframe: LoginWireframeInterface {
  func navigate(to option: LoginNavigationOption) {
    
  }
  

}
