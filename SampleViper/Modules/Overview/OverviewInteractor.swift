//
//  OverviewInteractor.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation
import Unbox

final class OverviewInteractor: OverviewInteractorInterface {

  // MARK: - Session
  
  func logout(completion: @escaping ((_ success: Bool, _ error: Error?)->())) {
    Session.shared.logout(completion: completion)
  }
  
  // MARK: - Data
  
  func getAnimals(completion: @escaping ((_ success: Bool, _ error: Error?, _ animals: [Animal]?)->())) {
    Session.shared.getAnimals { (success, error, json) in
        do {
          guard let json = json else {
            completion(false, error, nil)
            return
          }
          let animal: [Animal] = try unbox(dictionary: json, atKey: "data")
          completion(true, nil, animal)
        } catch let error {
          completion(false, error, nil)
        }
      }
  }
}
