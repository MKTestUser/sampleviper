//
//  OverviewInterfaces.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import Foundation


enum OverviewNavigationOption {
}

protocol OverviewWireframeInterface: WireframeInterface {
  func presentAddModule()
  func navigateToDetailsModule(item: Animal)
}

protocol OverviewViewInterface: ViewInterface, ResponseDisplayable {
  func refresh()
  func reloadCellAt(indexPath: IndexPath)
}

protocol OverviewPresenterInterface: PresenterInterface {
  func logout()
  func addAnimal()
  func removeAnimal(at path: IndexPath)
  func fetchAnimals()
  
  func prefetchImagesAt(paths: [IndexPath])
  func cancelImagePrefetchin(paths: [IndexPath])

  func numberOfRows() -> Int
  func itemAt(path: IndexPath) -> Animal?
  
  func didSelectItemAt(_ indexPath: IndexPath)
}

protocol OverviewInteractorInterface: InteractorInterface {
  func logout(completion: @escaping ((_ success: Bool, _ error: Error?)->()))
  func getAnimals(completion: @escaping ((_ success: Bool, _ error: Error?, _ animal: [Animal]?)->()))
}
