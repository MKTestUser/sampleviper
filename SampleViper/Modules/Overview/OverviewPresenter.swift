//
//  OverviewPresenter.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit
import Kingfisher

final class OverviewPresenter {
  
  // MARK: - Private properties -
  
  fileprivate unowned let _view: OverviewViewInterface
  fileprivate let _interactor: OverviewInteractorInterface
  fileprivate let _wireframe: OverviewWireframeInterface
  
  fileprivate var items: [Animal]?
  fileprivate var requests: [RetrieveImageDownloadTask]? = []
  
  
  // MARK: - Lifecycle -
  
  init(wireframe: OverviewWireframeInterface, view: OverviewViewInterface, interactor: OverviewInteractorInterface) {
    _wireframe = wireframe
    _view = view
    _interactor = interactor
  }
}

extension OverviewPresenter: OverviewPresenterInterface {
  
  // MARK: - Table view
  
  func prefetchImagesAt(paths: [IndexPath]) {
    for indexpath in paths {
      // prefetch request exists
      if let _ = requests?[safe: indexpath.item] {
        continue
      } else {
        // Starts download task, assigns image to animal on completion
        if let ani = items?[safe: indexpath.item],
          let imageUrl = ani.attributes?.imageUrl,
          let url = URL(string: imageUrl)?.asCustomURL(),
          let task = ImageDownloader.default.downloadImage(with: url, retrieveImageTask: nil, options: [.fromMemoryCacheOrRefresh], progressBlock: nil, completionHandler: { [weak self] (image, error, _, _) in
            if error == nil && indexpath.item < (self?.items?.count ?? 0) {
              self?.items?[indexpath.item].image = image
              // Reload cell with fade animation.
              self?._view.reloadCellAt(indexPath: indexpath)
            }
          }) {
          requests?.append(task)
        }
      }
    }
  }
  
  func cancelImagePrefetchin(paths: [IndexPath]) {
    var toDelete: [IndexPath] = []
    for indexpath in paths {
      if let request = requests?[safe: indexpath.item] {
        request.cancel()
        toDelete.append(indexpath)
      }
    }
    toDelete.forEach {
      if $0.item < (requests?.count ?? 0) {
        requests?.remove(at: $0.item)
      }
    }
  }
  
  func numberOfRows() -> Int {
    return items?.count ?? 0
  }
  
  func itemAt(path: IndexPath) -> Animal? {
    return items?[safe: path.row]
  }
  
  func didSelectItemAt(_ indexPath: IndexPath) {
    if let animal = items?[safe: indexPath.row] {
      _wireframe.navigateToDetailsModule(item: animal)
    }
  }
  
  // MARK: - View
  
  func viewDidLoad() {
    _view.startLoading()
    fetchAnimals()
  }
  
  // MARK: - Operations
  
  func logout() {
    _view.startLoading()
    _interactor.logout { [weak self] (success, error) in
      self?._view.stopLoading()
      if !success {
        self?._view.showAlert(status: OperationStatus.failed(error: error ?? NetworkError.generic), completion: nil)
      }
    }
  }
  
  // MARK: - Data
  
  func addAnimal() {
    _wireframe.presentAddModule()
  }
  
  func removeAnimal(at path: IndexPath) {
    guard let _ = items else {
      return
    }
    if path.row <= items!.count {
      items!.remove(at: path.row)
    }
  }
  
  func fetchAnimals() {
    _interactor.getAnimals { [weak self] (success, error, animal) in
      self?._view.stopLoading()
      if success {
        self?.items = animal
        self?._view.refresh()
      } else {
        self?._view.showAlert(status: OperationStatus.failed(error: error ?? NetworkError.generic), completion: nil)
      }
    }
  }
}
