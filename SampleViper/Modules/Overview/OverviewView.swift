//
//  OverviewView.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class OverviewView: BaseViewController {
  
  var presenter: OverviewPresenterInterface!
  
  lazy fileprivate var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .plain)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.backgroundColor = .white
    tableView.separatorColor = .clear
    tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    tableView.delegate = self
    tableView.dataSource = self
    tableView.prefetchDataSource = self
    tableView.allowsSelectionDuringEditing = false
    tableView.register(IconDescriptionCell.self, forCellReuseIdentifier: IconDescriptionCell.self.description())
    return tableView
  }()
  fileprivate let refreshControl = UIRefreshControl()

  // MARK: - View
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
    presenter.viewDidLoad()
  }
}

// MARK: - UITableViewDelegate

extension OverviewView: UITableViewDataSourcePrefetching {
  func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
    presenter.prefetchImagesAt(paths: indexPaths)
  }
  
  func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
    presenter.cancelImagePrefetchin(paths: indexPaths)
  }
}

// MARK: - UITableViewDelegate

extension OverviewView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    presenter.didSelectItemAt(indexPath)
    tableView.deselectRow(at: indexPath, animated: false)
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if (editingStyle == UITableViewCellEditingStyle.delete) {
      presenter.removeAnimal(at: indexPath)
      tableView.beginUpdates()
      tableView.deleteRows(at: [indexPath], with: .fade)
      tableView.endUpdates()
    }
  }
}

// MARK: - UITableViewDataSource

extension OverviewView: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.numberOfRows()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let animal = presenter.itemAt(path: indexPath),
      let animalCell = tableView.dequeueReusableCell(withIdentifier: IconDescriptionCell.self.description(), for: indexPath) as? IconDescriptionCell else {
      return UITableViewCell()
    }
    animalCell.populate(with: animal)
    animalCell.isSeparatorVisible = true
    return animalCell
  }
}

extension OverviewView: OverviewViewInterface {
  
  // MARK: - Setup
  
  func reloadCellAt(indexPath: IndexPath) {
    let indexPath = IndexPath(row: indexPath.row, section: 0)
    if self.tableView.indexPathsForVisibleRows?.contains(indexPath) ?? false {
      self.tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .fade)
    }
  }
  
  func refresh() {
    refreshControl.endRefreshing()
    tableView.reloadData()
  }
  
  // MARK: - Setup
  
  func setupViews() {
    
    refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    tableView.backgroundView = refreshControl
    
    // navigation
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-logout"), style: .plain, target: self, action: #selector(logout))
    navigationItem.leftBarButtonItem?.tintColor = .white
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-add"), style: .plain, target: self, action: #selector(addAnimal))
    navigationItem.rightBarButtonItem?.tintColor = .white
    navigationItem.title = "Animal ledger"
    
    // tableview
    view.addSubview(tableView)
  }
  
  func setupConstraints() {
    tableView.snp.makeConstraints { (make) in
      make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
      make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
      make.leading.trailing.equalToSuperview()
    }
  }
  
  // MARK: - User interaction
  
  @objc fileprivate func logout() {
    presenter.logout()
  }
  
  @objc fileprivate func addAnimal() {
    presenter.addAnimal()
  }
  
  @objc func refresh(_ refreshControl: UIRefreshControl) {
    presenter.fetchAnimals()
  }
}

