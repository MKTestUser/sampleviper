//
//  OverviewWireframe.swift
//  SampleViper
//
//  Created by SnowMan on 2018-06-07.
//  Copyright © 2018 Matej. All rights reserved.
//

import UIKit

final class OverviewWireframe: BaseWireframe {
  
  // MARK: - Module setup -
  
  static func assembleModule() -> BaseWireframe {
    let view = OverviewView()
    let interactor = OverviewInteractor()
    let router = OverviewWireframe(viewController: view)
    let presenter = OverviewPresenter(wireframe: router, view: view, interactor: interactor)
    // view
    view.presenter = presenter
    return router
  }
}


// MARK: - Extensions -

extension OverviewWireframe: OverviewWireframeInterface {
  func presentAddModule() {
    viewController.presentWireframe(AddWireframe.assembleModule())
  }

  func navigateToDetailsModule(item: Animal) {
    navigationController?.pushWireframe(DetailWireframe.assembleModule(with: item))
  }

}
